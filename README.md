# README #



### What is this repository for? ###

Object detection in videos with convolutional neural networks using Keras done for engineering thesis.
Based on: https://github.com/axon-research/c3d-keras - more details there
Consists of:
	- three examplary models
	- training script
	- testing scripts

### How do I get set up? ###

First pull repository https://github.com/axon-research/c3d-keras, and execute the instructions.
Then the scripts from this repository can be placed in the same directory. 
A dataset with training and testing videos should be acquired.
Scripts need txt files with video names and labels separtely. Each video/label should be in separate line.
Model to use is chosen with import statement (e.g. import c3d_model_new as c3d_model)
Hyperparameters can be chosen at the beginning of the script.
Training scripts also creates plots for losses and accuracies.
Testing scripts outputs accuracies for the whole set. 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact